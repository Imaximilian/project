import { Component, OnInit } from '@angular/core';
import { Todo } from '../class/todo';
import { TodoService } from '../todo.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  todoList: Todo[] = [];

  constructor(private todoService: TodoService) { }

  ngOnInit() {
    this.getTodos();
  }

  getTodos(){
    this.todoService.getTodos()
      .subscribe(todos => this.todoList = todos.slice(1, 5));
  }

}
