import { TodoService } from './../todo.service';
import { Todo } from './../class/todo';
import { Component, OnInit } from '@angular/core';




@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {

  todoList: Todo[];

  todo: Todo = {
    id: 1,
    name: 'Windstorm',
    description: 'hello'
  };


  constructor(private todoService: TodoService) { }

  ngOnInit() {
    this.getTodos();
  }

  getTodos(): void{
    this.todoService.getTodos()
      .subscribe(todos => this.todoList = todos);
  }

}
