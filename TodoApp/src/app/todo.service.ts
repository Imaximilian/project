import { todosList } from './mock-todos';
import { Injectable } from '@angular/core';
import { Todo } from './class/todo';
import { Observable } from 'rxjs';
import {of} from 'rxjs/observable/of.js'

@Injectable()
export class TodoService {

  constructor() { }

  getTodos(): Observable<Todo[]>{
    return of(todosList);
  }

  getTodo(id: number){
    return of(todosList.find(todo => todo.id ===id));
  }
}
